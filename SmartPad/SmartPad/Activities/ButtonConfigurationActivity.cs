﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SmartPad.DataBase;

namespace SmartPad.Activities
{
    [Activity(Label = "ButtonConfigurationActivity")]
    public class ButtonConfigurationActivity : Activity
    {
        DataBaseHelper db;
        BoardConf bc;
        List<Sound> soundList;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            int ID = Intent.GetIntExtra("ID", -1);
            if (ID == -1)
            {
                this.Finish();
            }

            db = new DataBaseHelper();
            SetContentView(Resource.Layout.activity_ButtonConfig);
            bc = db.GetBoardConfID(ID)[0];

            SeekBar redBar = FindViewById<SeekBar>(Resource.Id.seekBarRed);
            redBar.Progress = bc.Rot;
            redBar.ProgressChanged += seekBarProgressChanged;
            TextView tvR = FindViewById<TextView>(Resource.Id.textVeiwRed);
            tvR.Text = redBar.Progress.ToString();

            SeekBar greenBar = FindViewById<SeekBar>(Resource.Id.seekBarGreen);
            greenBar.Progress = bc.Gruen;
            greenBar.ProgressChanged += seekBarProgressChanged;
            TextView tvG = FindViewById<TextView>(Resource.Id.textViewGreen);
            tvG.Text = greenBar.Progress.ToString();

            SeekBar blueBar = FindViewById<SeekBar>(Resource.Id.seekBarBlue);
            blueBar.Progress = bc.Blau;
            blueBar.ProgressChanged += seekBarProgressChanged;
            TextView tvB = FindViewById<TextView>(Resource.Id.textViewBlue);
            tvB.Text = blueBar.Progress.ToString();

            soundList = db.GetAllSounds();

            List<string> nameList = new List<string>();
            nameList.Add("Default");
            foreach (Sound s in soundList)
            {
                nameList.Add(s.Name);
            }

            ListView lv = FindViewById<ListView>(Resource.Id.listViewSound);

            IListAdapter listAdapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, nameList);

            lv.Adapter = listAdapter;

            lv.ItemClick += listViewItemClick;

            Button okButton = FindViewById<Button>(Resource.Id.buttonOK);
            okButton.Click += okButtonOnClick;

            Button cancelButton = FindViewById<Button>(Resource.Id.buttonCancel);
            cancelButton.Click += cancelButtonOnClick;
            cancelButton.SetBackgroundColor(Color.Rgb(bc.Rot, bc.Gruen, bc.Blau));
            updateColor();
        }

        private void updateColor()
        {
            Button ok = FindViewById<Button>(Resource.Id.buttonOK);

            ok.SetBackgroundColor(Color.Rgb(bc.Rot, bc.Gruen, bc.Blau));

            if (bc.Rot + bc.Gruen + bc.Blau <= 150)
            {
                ok.SetTextColor(Color.White);
            }
            else
            {
                ok.SetTextColor(Color.Black);
            }

        }

        private void cancelButtonOnClick(object sender, EventArgs e)
        {
            StartActivity(typeof(SinglePlayerActivity));
            this.Finish();
        }

        private void okButtonOnClick(object sender, EventArgs e)
        {

            string message = db.UpdateBoardConfData(bc);
            Toast.MakeText(ApplicationContext, message, ToastLength.Long).Show();
            StartActivity(typeof(SinglePlayerActivity));
            this.Finish();
        }

        private void listViewItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (e.Position == 0)
            {
                bc.SoundID = -1;
            }
            else
            {
                bc.SoundID = e.Position;
            }
        }
        //test
        private void seekBarProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            SeekBar sb = sender as SeekBar;

            switch (sb.Id)
            {
                case Resource.Id.seekBarRed:
                    TextView tvR = FindViewById<TextView>(Resource.Id.textVeiwRed);
                    tvR.Text = sb.Progress.ToString();
                    bc.Rot = sb.Progress;
                    updateColor();
                    break;
                case Resource.Id.seekBarGreen:
                    TextView tvG = FindViewById<TextView>(Resource.Id.textViewGreen);
                    tvG.Text = sb.Progress.ToString();
                    bc.Gruen = sb.Progress;
                    updateColor();
                    break;
                case Resource.Id.seekBarBlue:
                    TextView tvB = FindViewById<TextView>(Resource.Id.textViewBlue);
                    tvB.Text = sb.Progress.ToString();
                    bc.Blau = sb.Progress;
                    updateColor();
                    break;
            }


        }
    }
}