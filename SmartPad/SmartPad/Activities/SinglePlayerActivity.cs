﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SmartPad.DataBase;
using SmartPad.Utils;

namespace SmartPad.Activities
{
    [Activity(Label = "SinglePlayerActivity")]
    public class SinglePlayerActivity : Activity
    {

        //public MediaPlayer player;
        public SoundPool soundPool;
        public SoundPool.Builder spBuilder;
        public AudioAttributes audioAttributes;
        
        DataBaseHelper db;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            db = new DataBaseHelper();
            SetContentView(Resource.Layout.activity_singlePlayer);

            spBuilder = new SoundPool.Builder();
            spBuilder.SetMaxStreams(10);
            spBuilder.SetAudioAttributes(new AudioAttributes.Builder().SetContentType(AudioContentType.Music).SetUsage(AudioUsageKind.Media).Build());
            
            soundPool = spBuilder.Build();
            buildButtonGrid();
            // Create your application here
        }
        private void buildButtonGrid()
        {
            TableLayout tl = (TableLayout)this.FindViewById(Resource.Id.tableLayout);
            List<BoardConf> bcl = db.GetAllBoardConf();
            TableRow tr;
            TableLayout.LayoutParams trlayout = new TableLayout.LayoutParams(TableLayout.LayoutParams.MatchParent, TableLayout.LayoutParams.MatchParent);
            trlayout.Gravity = GravityFlags.Center;
            trlayout.Weight = 1;
            int btIdCount = 0;
            if (bcl.Count == 0)
            {
                while (btIdCount < 24)
                {
                    BoardConf bc = new BoardConf();
                    bc.ID = btIdCount;
                    bc.Rot = 153;
                    bc.Gruen = 153;
                    bc.Blau = 153;
                    bc.SoundID = -1;
                    bc.Stop = true;
                    bc.Repeate = false;
                    db.InsertUpdateBoardConfData(bc);
                    btIdCount++;
                    
                }
                btIdCount = 0;
                bcl = db.GetAllBoardConf();
            }
            for (int i = 0; i < 6; i++)
            {
                tr = new TableRow(this);
                //var tr = LayoutInflater.Inflate(Resource.Layout.row, tl, true);
                tr.LayoutParameters = trlayout;

                tl.AddView(tr);
                //tr.LayoutParameters = layoutParams;
                for (int j = 0; j < 4; j++)
                {
                    BoardConf boardConf = bcl[btIdCount];
                    SoundButton but;
                    if (boardConf.SoundID == -1)
                    {
                        but = new SoundButton(this, true, btIdCount);
                        LoadSoundDefault(btIdCount);
                    }
                    else
                    {
                        Sound s = db.GetSoundID(boardConf.SoundID)[0];
                        LoadSoundFromDatatabase(btIdCount, s);
                        but = new SoundButton(this, true, s, btIdCount);
                    }


                    //SoundButton but = new SoundButton(this, false, "test", 1);

                    but.SetBackgroundColor(Color.Rgb(boardConf.Rot, boardConf.Gruen, boardConf.Blau));

                    btIdCount++;
                    //sb.SetWidth(Resources.DisplayMetrics.WidthPixels/4-20);
                    //sb.SetHeight(sb.MaxWidth);
                    TableRow.LayoutParams param = new TableRow.LayoutParams(TableRow.LayoutParams.MatchParent, TableRow.LayoutParams.MatchParent);
                    //sb.LayoutParameters = new TableRow.LayoutParams(j);
                    param.SetMargins(5, 5, 5, 5);
                    but.SoundEffectsEnabled = false;
                    but.LayoutParameters = param;

                    but.Click += sbOnClick;
                    but.LongClick += sbOnLongClick;
                    tr.AddView(but);

                }
            }
        }

        private void LoadSoundFromDatatabase(int btIdCount, Sound s)
        {
            soundPool.Load(s.Path, 1);
        }

        private void LoadSoundDefault(int btIdCount)
        {
            var fields = typeof(Resource.Raw).GetFields();

            string s = fields[btIdCount].Name;

            int fileid = (int)typeof(Resource.Raw).GetField(s).GetValue(null);

            soundPool.Load(this, fileid, 1);
        }

        private void sbOnLongClick(object sender, View.LongClickEventArgs e)
        {
            SoundButton sb = sender as SoundButton;
            var buttonConfActivity = new Intent(this, typeof(ButtonConfigurationActivity));
            buttonConfActivity.PutExtra("ID", sb.sId);
            StartActivity(buttonConfActivity);
            this.Finish();
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
            soundPool.Release();
        }
        private void sbOnClick(object sender, EventArgs e)
        {
            SoundButton sb = sender as SoundButton;
            if (sb.stop == true)
            {
                soundPool.Stop(sb.spId);
            }
            sb.spId = soundPool.Play(sb.sId+1, 1, 1, 0, 0, 1);
                //player = MediaPlayer.Create(this, fileid);

            //player.Start();
            //player.Completion += delegate { player.Release(); };
        }
    }
}