﻿using Android.App;
using Android.Content;
using Android.Net.Wifi.P2p;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SmartPad.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartPad.Activities
{
    [Activity(Label = "LobbyActivity")]
    public class LobbyActivity : Activity, WifiP2pManager.IChannelListener, WifiP2pManager.IPeerListListener, WifiP2pManager.IConnectionInfoListener, Interfaces.IDeviceActionListener
    {
        public WifiP2pManager manager;
        public WifiP2pManager.Channel channel;
        public BroadcastReceiver receiver;
        public WifiP2pInfo info;

        private WifiP2pDevice device;

        private readonly List<WifiP2pDevice> peers = new List<WifiP2pDevice>();

        private readonly IntentFilter intentFilter = new IntentFilter();

        public ProgressBar progressBar;

        ListView lv;

        private bool retryChannel; //um dem gerät einen 2ten verbindungsversuch zu erlauben

        public bool IsWifiP2PEnabled { get; set; }
        public const string Tag = "lobby";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_lobby);

            //P2P Initialisierung
            manager = (WifiP2pManager)GetSystemService(Context.WifiP2pService);
            channel = manager.Initialize(this, MainLooper, null);
            receiver = new Services.WiFiDirectBroadcastReceiver(manager, channel, this);

            //Intentfilter
            intentFilter.AddAction(WifiP2pManager.WifiP2pStateChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pConnectionChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pPeersChangedAction);
            intentFilter.AddAction(WifiP2pManager.WifiP2pThisDeviceChangedAction);

            //Progressbar
            progressBar = new ProgressBar(this, null, Android.Resource.Attribute.ProgressBarStyleHorizontal);
            LinearLayout layout = FindViewById<LinearLayout>(Resource.Id.ConnectionLayout);
            layout.AddView(progressBar, 0);
            progressBar.Indeterminate = true;
            progressBar.Visibility = ViewStates.Gone;

            //IO Manipulation
            Button buttonDisconnect = FindViewById<Button>(Resource.Id.buttonDisconnect);
            buttonDisconnect.Visibility = ViewStates.Gone;
            buttonDisconnect.Click += onDisconnectButtonClick;

            Button buttonStartMP = FindViewById<Button>(Resource.Id.buttonStartMP);
            buttonStartMP.Visibility = ViewStates.Gone;
            buttonStartMP.Click += onStartMPButtonClick;

            Button buttonTestStream = FindViewById<Button>(Resource.Id.buttonStartStream);
            buttonTestStream.Visibility = ViewStates.Gone;
            buttonTestStream.Click += onTestStreamClick;

            Button buttonDiscover = FindViewById<Button>(Resource.Id.buttonDiscover);
            buttonDiscover.Click += onButtonDiscoverClick;

            lv = FindViewById<ListView>(Resource.Id.listViewHost);
            lv.ItemClick += onListViewItemClick;
        }

        private void onButtonDiscoverClick(object sender, EventArgs e)
        {
            onInitiateDiscovery();
            manager.DiscoverPeers(channel, new MyActionListner(this, "Discovery", () => { }));
        }

        private void onInitiateDiscovery()
        {
            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;
            progressBar.Visibility = ViewStates.Visible;
        }

        protected override void OnResume()
        {
            base.OnResume();
            RegisterReceiver(receiver, intentFilter);
        }

        private void onListViewItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            LayoutInflater layoutInflater = LayoutInflater.From(this);
            View view = layoutInflater.Inflate(Resource.Layout.connectionDialog, null);
            AlertDialog.Builder alertbuilder = new AlertDialog.Builder(this);

            alertbuilder.SetView(view);
            alertbuilder.SetCancelable(false)
                .SetPositiveButton("Connect", ConnectButtonClick)
                .SetNegativeButton("Cancel", delegate
                {
                    alertbuilder.Dispose();
                }).SetTitle(peers[e.Position].DeviceName);
            device = peers[e.Position];
            AlertDialog alertDialog = alertbuilder.Create();
            alertDialog.Show();
        }

        private void ConnectButtonClick(object sender, DialogClickEventArgs e)
        {
            var config = new WifiP2pConfig();
            config.DeviceAddress = device.DeviceAddress;
            CheckBox checkBox = FindViewById<CheckBox>(Resource.Id.checkBoxHost);
            if (checkBox.Checked)
            {
                config.GroupOwnerIntent = 15;
            }
            else
            {
                config.GroupOwnerIntent = 0;
            }
            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;

            Toast.MakeText(this, "Connecting to: " + device.DeviceAddress, ToastLength.Short).Show();

            ((IDeviceActionListener)this).Connect(config);

        }

        private void onTestStreamClick(object sender, EventArgs e)
        {
            
        }

        private void onStartMPButtonClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void onDisconnectButtonClick(object sender, EventArgs e)
        {
            Disconnect();
        }
        protected override void OnPause()
        {
            base.OnPause();
            UnregisterReceiver(receiver);
        }

        public void OnChannelDisconnected()
        {
            // we will try once more
            if (manager != null && !retryChannel)
            {
                Toast.MakeText(this, "Channel lost. Trying again", ToastLength.Long).Show();
                ResetData();
                retryChannel = true;
                manager.Initialize(this, MainLooper, this);
            }
            else
            {
                Toast.MakeText(this, "Severe! Channel is probably lost permanently. Try Disable/Re-Enable P2P.",
                               ToastLength.Long).Show();
            }
        }

        public void OnPeersAvailable(WifiP2pDeviceList peers)
        {
            HideProgressBar();

            this.peers.Clear();
            List<string> nameList = new List<string>();
            ListView lv = FindViewById<ListView>(Resource.Id.listViewHost);
            foreach (var peer in peers.DeviceList)
            {
                this.peers.Add(peer);
            }
            foreach (var peer in this.peers)
            {
                nameList.Add(peer.DeviceName);
            }
            IListAdapter listAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, nameList);
            lv.Adapter = listAdapter;
        }

        public void OnConnectionInfoAvailable(WifiP2pInfo info)
        {
            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;

            this.info = info;

            Toast.MakeText(this, (info.IsGroupOwner) ? "You are th Host. Your IP: " + info.GroupOwnerAddress.HostAddress : "You are the Client. Host IP: " + info.GroupOwnerAddress.HostAddress, ToastLength.Long).Show();

            if (info.GroupFormed && info.IsGroupOwner)
            {
                
                LinearLayout layout = FindViewById<LinearLayout>(Resource.Id.discoverLayout);
                Button bt = FindViewById<Button>(Resource.Id.buttonDisconnect);
                bt.Visibility = ViewStates.Visible;
                TextView tv = FindViewById<TextView>(Resource.Id.textboxClientIP);
                tv.Visibility = ViewStates.Visible;
                tv.Text = "Host IP: " + info.GroupOwnerAddress.HostAddress;
                var serverThread = new Services.ServerThread(this);
                serverThread.Start();
                
            }
            else if (info.GroupFormed)
            {
                Button startStream = FindViewById<Button>(Resource.Id.buttonStartStream);
                startStream.Visibility = ViewStates.Visible;
                LinearLayout layout = FindViewById<LinearLayout>(Resource.Id.discoverLayout);
                layout.Visibility = ViewStates.Gone;
                Button buttonDisconnect = FindViewById<Button>(Resource.Id.buttonDisconnect);
                buttonDisconnect.Visibility = ViewStates.Visible;
                TextView tv = FindViewById<TextView>(Resource.Id.textboxClientIP);
                tv.Visibility = ViewStates.Visible;
                tv.Text = "Host IP: " + info.GroupOwnerAddress.HostAddress;
                var clientThread = new Services.ClientThread(info.GroupOwnerAddress.HostAddress, this);
                clientThread.Start();
            }
        }

        public void UpdateThisDevice(WifiP2pDevice wifiP2pDevice)
        {
            device = wifiP2pDevice;
            Toast.MakeText(this, "Connected to: " + wifiP2pDevice.DeviceName, ToastLength.Short);
        }
        
        private void Disconnect()
        {
            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;


            manager.RemoveGroup(channel, new MyActionListner(this, "Disconnect", () =>
            {
                Button bt = FindViewById<Button>(Resource.Id.buttonStartMP);
                bt.Visibility = ViewStates.Gone;
                LinearLayout layout = FindViewById<LinearLayout>(Resource.Id.discoverLayout);
                layout.Visibility = ViewStates.Visible;
                bt = FindViewById<Button>(Resource.Id.buttonDisconnect);
                bt.Visibility = ViewStates.Gone;
            }));
        }

        internal void ResetData()
        {
            if (lv != null)
            {
                peers.Clear();
            }
        }

        public void CancelDisconnect()
        {
            throw new NotImplementedException();
        }

        public void Connect(WifiP2pConfig config)
        {
            manager.Connect(channel, config, new MyActionListner(this, "Connect", () => { }));
        }

        void IDeviceActionListener.Disconnect()
        {
            throw new NotImplementedException();
        }

        private void HideProgressBar()
        {
            if (progressBar != null && progressBar.Visibility == ViewStates.Visible)
                progressBar.Visibility = ViewStates.Gone;
        }

        private void RevealProgressBar()
        {
            if (progressBar != null && progressBar.Visibility == ViewStates.Gone)
                progressBar.Visibility = ViewStates.Visible;
        }

        
    }
}