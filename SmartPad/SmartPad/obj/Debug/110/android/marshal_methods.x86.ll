; ModuleID = 'obj\Debug\110\android\marshal_methods.x86.ll'
source_filename = "obj\Debug\110\android\marshal_methods.x86.ll"
target datalayout = "e-m:e-p:32:32-p270:32:32-p271:32:32-p272:64:64-f64:32:64-f80:32-n8:16:32-S128"
target triple = "i686-unknown-linux-android"


%struct.MonoImage = type opaque

%struct.MonoClass = type opaque

%struct.MarshalMethodsManagedClass = type {
	i32,; uint32_t token
	%struct.MonoClass*; MonoClass* klass
}

%struct.MarshalMethodName = type {
	i64,; uint64_t id
	i8*; char* name
}

%class._JNIEnv = type opaque

%class._jobject = type {
	i8; uint8_t b
}

%class._jclass = type {
	i8; uint8_t b
}

%class._jstring = type {
	i8; uint8_t b
}

%class._jthrowable = type {
	i8; uint8_t b
}

%class._jarray = type {
	i8; uint8_t b
}

%class._jobjectArray = type {
	i8; uint8_t b
}

%class._jbooleanArray = type {
	i8; uint8_t b
}

%class._jbyteArray = type {
	i8; uint8_t b
}

%class._jcharArray = type {
	i8; uint8_t b
}

%class._jshortArray = type {
	i8; uint8_t b
}

%class._jintArray = type {
	i8; uint8_t b
}

%class._jlongArray = type {
	i8; uint8_t b
}

%class._jfloatArray = type {
	i8; uint8_t b
}

%class._jdoubleArray = type {
	i8; uint8_t b
}

; assembly_image_cache
@assembly_image_cache = local_unnamed_addr global [0 x %struct.MonoImage*] zeroinitializer, align 4
; Each entry maps hash of an assembly name to an index into the `assembly_image_cache` array
@assembly_image_cache_hashes = local_unnamed_addr constant [136 x i32] [
	i32 57967248, ; 0: Xamarin.Android.Support.VersionedParcelable.dll => 0x3748290 => 51
	i32 160529393, ; 1: Xamarin.Android.Arch.Core.Common => 0x9917bf1 => 18
	i32 166922606, ; 2: Xamarin.Android.Support.Compat.dll => 0x9f3096e => 29
	i32 201930040, ; 3: Xamarin.Android.Arch.Core.Runtime => 0xc093538 => 19
	i32 232815796, ; 4: System.Web.Services => 0xde07cb4 => 65
	i32 293914992, ; 5: Xamarin.Android.Support.Transition => 0x1184c970 => 46
	i32 321597661, ; 6: System.Numerics => 0x132b30dd => 13
	i32 347068432, ; 7: SQLitePCLRaw.lib.e_sqlite3.android.dll => 0x14afd810 => 7
	i32 385762202, ; 8: System.Memory.dll => 0x16fe439a => 12
	i32 388313361, ; 9: Xamarin.Android.Support.Animated.Vector.Drawable => 0x17253111 => 25
	i32 389971796, ; 10: Xamarin.Android.Support.Core.UI => 0x173e7f54 => 31
	i32 442521989, ; 11: Xamarin.Essentials => 0x1a605985 => 53
	i32 465846621, ; 12: mscorlib => 0x1bc4415d => 3
	i32 469710990, ; 13: System.dll => 0x1bff388e => 11
	i32 514659665, ; 14: Xamarin.Android.Support.Compat => 0x1ead1551 => 29
	i32 524864063, ; 15: Xamarin.Android.Support.Print => 0x1f48ca3f => 43
	i32 526420162, ; 16: System.Transactions.dll => 0x1f6088c2 => 58
	i32 539750087, ; 17: Xamarin.Android.Support.Design => 0x202beec7 => 36
	i32 571524804, ; 18: Xamarin.Android.Support.v7.RecyclerView => 0x2210c6c4 => 49
	i32 605376203, ; 19: System.IO.Compression.FileSystem => 0x24154ecb => 61
	i32 690569205, ; 20: System.Xml.Linq.dll => 0x29293ff5 => 66
	i32 692692150, ; 21: Xamarin.Android.Support.Annotations => 0x2949a4b6 => 26
	i32 748832960, ; 22: SQLitePCLRaw.batteries_v2 => 0x2ca248c0 => 5
	i32 775507847, ; 23: System.IO.Compression => 0x2e394f87 => 60
	i32 801787702, ; 24: Xamarin.Android.Support.Interpolator => 0x2fca4f36 => 40
	i32 809851609, ; 25: System.Drawing.Common.dll => 0x30455ad9 => 55
	i32 916714535, ; 26: Xamarin.Android.Support.Print.dll => 0x36a3f427 => 43
	i32 987342438, ; 27: Xamarin.Android.Arch.Lifecycle.LiveData.dll => 0x3ad9a666 => 22
	i32 1098167829, ; 28: Xamarin.Android.Arch.Lifecycle.ViewModel => 0x4174b615 => 24
	i32 1098259244, ; 29: System => 0x41761b2c => 11
	i32 1292207520, ; 30: SQLitePCLRaw.core.dll => 0x4d0585a0 => 6
	i32 1292763917, ; 31: Xamarin.Android.Support.CursorAdapter.dll => 0x4d0e030d => 33
	i32 1297413738, ; 32: Xamarin.Android.Arch.Lifecycle.LiveData.Core => 0x4d54f66a => 21
	i32 1359785034, ; 33: Xamarin.Android.Support.Design.dll => 0x510cac4a => 36
	i32 1365406463, ; 34: System.ServiceModel.Internals.dll => 0x516272ff => 64
	i32 1411638395, ; 35: System.Runtime.CompilerServices.Unsafe => 0x5423e47b => 15
	i32 1445445088, ; 36: Xamarin.Android.Support.Fragment => 0x5627bde0 => 39
	i32 1462112819, ; 37: System.IO.Compression.dll => 0x57261233 => 60
	i32 1574652163, ; 38: Xamarin.Android.Support.Core.Utils.dll => 0x5ddb4903 => 32
	i32 1587447679, ; 39: Xamarin.Android.Arch.Core.Common.dll => 0x5e9e877f => 18
	i32 1592978981, ; 40: System.Runtime.Serialization.dll => 0x5ef2ee25 => 63
	i32 1639515021, ; 41: System.Net.Http.dll => 0x61b9038d => 54
	i32 1657153582, ; 42: System.Runtime => 0x62c6282e => 16
	i32 1662014763, ; 43: Xamarin.Android.Support.Vector.Drawable => 0x6310552b => 50
	i32 1711441057, ; 44: SQLitePCLRaw.lib.e_sqlite3.android => 0x660284a1 => 7
	i32 1776026572, ; 45: System.Core.dll => 0x69dc03cc => 10
	i32 1866717392, ; 46: Xamarin.Android.Support.Interpolator.dll => 0x6f43d8d0 => 40
	i32 1867746548, ; 47: Xamarin.Essentials.dll => 0x6f538cf4 => 53
	i32 1877418711, ; 48: Xamarin.Android.Support.v7.RecyclerView.dll => 0x6fe722d7 => 49
	i32 1916660109, ; 49: Xamarin.Android.Arch.Lifecycle.ViewModel.dll => 0x723de98d => 24
	i32 2011961780, ; 50: System.Buffers.dll => 0x77ec19b4 => 9
	i32 2037417872, ; 51: Xamarin.Android.Support.ViewPager => 0x79708790 => 52
	i32 2044222327, ; 52: Xamarin.Android.Support.Loader => 0x79d85b77 => 41
	i32 2079903147, ; 53: System.Runtime.dll => 0x7bf8cdab => 16
	i32 2090596640, ; 54: System.Numerics.Vectors => 0x7c9bf920 => 14
	i32 2103459038, ; 55: SQLitePCLRaw.provider.e_sqlite3.dll => 0x7d603cde => 8
	i32 2139458754, ; 56: Xamarin.Android.Support.DrawerLayout => 0x7f858cc2 => 38
	i32 2166116741, ; 57: Xamarin.Android.Support.Core.Utils => 0x811c5185 => 32
	i32 2196165013, ; 58: Xamarin.Android.Support.VersionedParcelable => 0x82e6d195 => 51
	i32 2201231467, ; 59: System.Net.Http => 0x8334206b => 54
	i32 2330457430, ; 60: Xamarin.Android.Support.Core.UI.dll => 0x8ae7f556 => 31
	i32 2330986192, ; 61: Xamarin.Android.Support.SlidingPaneLayout.dll => 0x8af006d0 => 44
	i32 2373288475, ; 62: Xamarin.Android.Support.Fragment.dll => 0x8d75821b => 39
	i32 2440966767, ; 63: Xamarin.Android.Support.Loader.dll => 0x917e326f => 41
	i32 2465273461, ; 64: SQLitePCLRaw.batteries_v2.dll => 0x92f11675 => 5
	i32 2471841756, ; 65: netstandard.dll => 0x93554fdc => 56
	i32 2475788418, ; 66: Java.Interop.dll => 0x93918882 => 1
	i32 2487632829, ; 67: Xamarin.Android.Support.DocumentFile => 0x944643bd => 37
	i32 2501346920, ; 68: System.Data.DataSetExtensions => 0x95178668 => 59
	i32 2662659795, ; 69: SmartPad => 0x9eb4f6d3 => 0
	i32 2698266930, ; 70: Xamarin.Android.Arch.Lifecycle.LiveData => 0xa0d44932 => 22
	i32 2699883881, ; 71: SmartPad.dll => 0xa0ecf569 => 0
	i32 2751899777, ; 72: Xamarin.Android.Support.Collections => 0xa406a881 => 28
	i32 2782647110, ; 73: Xamarin.Android.Support.CustomTabs.dll => 0xa5dbd346 => 34
	i32 2788639665, ; 74: Xamarin.Android.Support.LocalBroadcastManager => 0xa63743b1 => 42
	i32 2788775637, ; 75: Xamarin.Android.Support.SwipeRefreshLayout.dll => 0xa63956d5 => 45
	i32 2819470561, ; 76: System.Xml.dll => 0xa80db4e1 => 17
	i32 2876493027, ; 77: Xamarin.Android.Support.SwipeRefreshLayout => 0xab73cce3 => 45
	i32 2893257763, ; 78: Xamarin.Android.Arch.Core.Runtime.dll => 0xac739c23 => 19
	i32 2903344695, ; 79: System.ComponentModel.Composition => 0xad0d8637 => 62
	i32 2905242038, ; 80: mscorlib.dll => 0xad2a79b6 => 3
	i32 2919462931, ; 81: System.Numerics.Vectors.dll => 0xae037813 => 14
	i32 2921692953, ; 82: Xamarin.Android.Support.CustomView.dll => 0xae257f19 => 35
	i32 2922925221, ; 83: Xamarin.Android.Support.Vector.Drawable.dll => 0xae384ca5 => 50
	i32 3056250942, ; 84: Xamarin.Android.Support.AsyncLayoutInflater.dll => 0xb62ab03e => 27
	i32 3068715062, ; 85: Xamarin.Android.Arch.Lifecycle.Common => 0xb6e8e036 => 20
	i32 3111772706, ; 86: System.Runtime.Serialization => 0xb979e222 => 63
	i32 3191408315, ; 87: Xamarin.Android.Support.CustomTabs => 0xbe3906bb => 34
	i32 3204380047, ; 88: System.Data.dll => 0xbefef58f => 57
	i32 3204912593, ; 89: Xamarin.Android.Support.AsyncLayoutInflater => 0xbf0715d1 => 27
	i32 3233339011, ; 90: Xamarin.Android.Arch.Lifecycle.LiveData.Core.dll => 0xc0b8d683 => 21
	i32 3247949154, ; 91: Mono.Security => 0xc197c562 => 67
	i32 3286872994, ; 92: SQLite-net.dll => 0xc3e9b3a2 => 4
	i32 3296380511, ; 93: Xamarin.Android.Support.Collections.dll => 0xc47ac65f => 28
	i32 3317144872, ; 94: System.Data => 0xc5b79d28 => 57
	i32 3321585405, ; 95: Xamarin.Android.Support.DocumentFile.dll => 0xc5fb5efd => 37
	i32 3352662376, ; 96: Xamarin.Android.Support.CoordinaterLayout => 0xc7d59168 => 30
	i32 3357663996, ; 97: Xamarin.Android.Support.CursorAdapter => 0xc821e2fc => 33
	i32 3360279109, ; 98: SQLitePCLRaw.core => 0xc849ca45 => 6
	i32 3366347497, ; 99: Java.Interop => 0xc8a662e9 => 1
	i32 3395150330, ; 100: System.Runtime.CompilerServices.Unsafe.dll => 0xca5de1fa => 15
	i32 3404865022, ; 101: System.ServiceModel.Internals => 0xcaf21dfe => 64
	i32 3429136800, ; 102: System.Xml => 0xcc6479a0 => 17
	i32 3430777524, ; 103: netstandard => 0xcc7d82b4 => 56
	i32 3439690031, ; 104: Xamarin.Android.Support.Annotations.dll => 0xcd05812f => 26
	i32 3476120550, ; 105: Mono.Android => 0xcf3163e6 => 2
	i32 3486566296, ; 106: System.Transactions => 0xcfd0c798 => 58
	i32 3498942916, ; 107: Xamarin.Android.Support.v7.CardView.dll => 0xd08da1c4 => 48
	i32 3509114376, ; 108: System.Xml.Linq => 0xd128d608 => 66
	i32 3547625832, ; 109: Xamarin.Android.Support.SlidingPaneLayout => 0xd3747968 => 44
	i32 3567349600, ; 110: System.ComponentModel.Composition.dll => 0xd4a16f60 => 62
	i32 3664423555, ; 111: Xamarin.Android.Support.ViewPager.dll => 0xda6aaa83 => 52
	i32 3672681054, ; 112: Mono.Android.dll => 0xdae8aa5e => 2
	i32 3676310014, ; 113: System.Web.Services.dll => 0xdb2009fe => 65
	i32 3678221644, ; 114: Xamarin.Android.Support.v7.AppCompat => 0xdb3d354c => 47
	i32 3681174138, ; 115: Xamarin.Android.Arch.Lifecycle.Common.dll => 0xdb6a427a => 20
	i32 3689375977, ; 116: System.Drawing.Common => 0xdbe768e9 => 55
	i32 3714038699, ; 117: Xamarin.Android.Support.LocalBroadcastManager.dll => 0xdd5fbbab => 42
	i32 3718463572, ; 118: Xamarin.Android.Support.Animated.Vector.Drawable.dll => 0xdda34054 => 25
	i32 3754567612, ; 119: SQLitePCLRaw.provider.e_sqlite3 => 0xdfca27bc => 8
	i32 3776062606, ; 120: Xamarin.Android.Support.DrawerLayout.dll => 0xe112248e => 38
	i32 3829621856, ; 121: System.Numerics.dll => 0xe4436460 => 13
	i32 3832731800, ; 122: Xamarin.Android.Support.CoordinaterLayout.dll => 0xe472d898 => 30
	i32 3862817207, ; 123: Xamarin.Android.Arch.Lifecycle.Runtime.dll => 0xe63de9b7 => 23
	i32 3874897629, ; 124: Xamarin.Android.Arch.Lifecycle.Runtime => 0xe6f63edd => 23
	i32 3876362041, ; 125: SQLite-net => 0xe70c9739 => 4
	i32 3883175360, ; 126: Xamarin.Android.Support.v7.AppCompat.dll => 0xe7748dc0 => 47
	i32 3920810846, ; 127: System.IO.Compression.FileSystem.dll => 0xe9b2d35e => 61
	i32 3945713374, ; 128: System.Data.DataSetExtensions.dll => 0xeb2ecede => 59
	i32 4025784931, ; 129: System.Memory => 0xeff49a63 => 12
	i32 4063435586, ; 130: Xamarin.Android.Support.CustomView => 0xf2331b42 => 35
	i32 4105002889, ; 131: Mono.Security.dll => 0xf4ad5f89 => 67
	i32 4151237749, ; 132: System.Core => 0xf76edc75 => 10
	i32 4216993138, ; 133: Xamarin.Android.Support.Transition.dll => 0xfb5a3572 => 46
	i32 4219003402, ; 134: Xamarin.Android.Support.v7.CardView => 0xfb78e20a => 48
	i32 4260525087 ; 135: System.Buffers => 0xfdf2741f => 9
], align 4
@assembly_image_cache_indices = local_unnamed_addr constant [136 x i32] [
	i32 51, i32 18, i32 29, i32 19, i32 65, i32 46, i32 13, i32 7, ; 0..7
	i32 12, i32 25, i32 31, i32 53, i32 3, i32 11, i32 29, i32 43, ; 8..15
	i32 58, i32 36, i32 49, i32 61, i32 66, i32 26, i32 5, i32 60, ; 16..23
	i32 40, i32 55, i32 43, i32 22, i32 24, i32 11, i32 6, i32 33, ; 24..31
	i32 21, i32 36, i32 64, i32 15, i32 39, i32 60, i32 32, i32 18, ; 32..39
	i32 63, i32 54, i32 16, i32 50, i32 7, i32 10, i32 40, i32 53, ; 40..47
	i32 49, i32 24, i32 9, i32 52, i32 41, i32 16, i32 14, i32 8, ; 48..55
	i32 38, i32 32, i32 51, i32 54, i32 31, i32 44, i32 39, i32 41, ; 56..63
	i32 5, i32 56, i32 1, i32 37, i32 59, i32 0, i32 22, i32 0, ; 64..71
	i32 28, i32 34, i32 42, i32 45, i32 17, i32 45, i32 19, i32 62, ; 72..79
	i32 3, i32 14, i32 35, i32 50, i32 27, i32 20, i32 63, i32 34, ; 80..87
	i32 57, i32 27, i32 21, i32 67, i32 4, i32 28, i32 57, i32 37, ; 88..95
	i32 30, i32 33, i32 6, i32 1, i32 15, i32 64, i32 17, i32 56, ; 96..103
	i32 26, i32 2, i32 58, i32 48, i32 66, i32 44, i32 62, i32 52, ; 104..111
	i32 2, i32 65, i32 47, i32 20, i32 55, i32 42, i32 25, i32 8, ; 112..119
	i32 38, i32 13, i32 30, i32 23, i32 23, i32 4, i32 47, i32 61, ; 120..127
	i32 59, i32 12, i32 35, i32 67, i32 10, i32 46, i32 48, i32 9 ; 136..135
], align 4

@marshal_methods_number_of_classes = local_unnamed_addr constant i32 0, align 4

; marshal_methods_class_cache
@marshal_methods_class_cache = global [0 x %struct.MarshalMethodsManagedClass] [
], align 4; end of 'marshal_methods_class_cache' array


@get_function_pointer = internal unnamed_addr global void (i32, i32, i32, i8**)* null, align 4

; Function attributes: "frame-pointer"="none" "min-legal-vector-width"="0" mustprogress nofree norecurse nosync "no-trapping-math"="true" nounwind sspstrong "stack-protector-buffer-size"="8" "stackrealign" "target-cpu"="i686" "target-features"="+cx8,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "tune-cpu"="generic" uwtable willreturn writeonly
define void @xamarin_app_init (void (i32, i32, i32, i8**)* %fn) local_unnamed_addr #0
{
	store void (i32, i32, i32, i8**)* %fn, void (i32, i32, i32, i8**)** @get_function_pointer, align 4
	ret void
}

; Names of classes in which marshal methods reside
@mm_class_names = local_unnamed_addr constant [0 x i8*] zeroinitializer, align 4
@__MarshalMethodName_name.0 = internal constant [1 x i8] c"\00", align 1

; mm_method_names
@mm_method_names = local_unnamed_addr constant [1 x %struct.MarshalMethodName] [
	; 0
	%struct.MarshalMethodName {
		i64 0, ; id 0x0; name: 
		i8* getelementptr inbounds ([1 x i8], [1 x i8]* @__MarshalMethodName_name.0, i32 0, i32 0); name
	}
], align 8; end of 'mm_method_names' array


attributes #0 = { "min-legal-vector-width"="0" mustprogress nofree norecurse nosync "no-trapping-math"="true" nounwind sspstrong "stack-protector-buffer-size"="8" uwtable willreturn writeonly "frame-pointer"="none" "target-cpu"="i686" "target-features"="+cx8,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "tune-cpu"="generic" "stackrealign" }
attributes #1 = { "min-legal-vector-width"="0" mustprogress "no-trapping-math"="true" nounwind sspstrong "stack-protector-buffer-size"="8" uwtable "frame-pointer"="none" "target-cpu"="i686" "target-features"="+cx8,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "tune-cpu"="generic" "stackrealign" }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}
!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{i32 1, !"NumRegisterParameters", i32 0}
!3 = !{!"Xamarin.Android remotes/origin/d17-5 @ a200af12c1e846626b8caebd926ac14c185f71ec"}
!llvm.linker.options = !{}
