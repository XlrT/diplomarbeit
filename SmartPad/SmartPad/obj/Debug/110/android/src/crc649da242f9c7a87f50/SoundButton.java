package crc649da242f9c7a87f50;


public class SoundButton
	extends android.widget.Button
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("SmartPad.Utils.SoundButton, SmartPad", SoundButton.class, __md_methods);
	}


	public SoundButton (android.content.Context p0)
	{
		super (p0);
		if (getClass () == SoundButton.class) {
			mono.android.TypeManager.Activate ("SmartPad.Utils.SoundButton, SmartPad", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
		}
	}


	public SoundButton (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == SoundButton.class) {
			mono.android.TypeManager.Activate ("SmartPad.Utils.SoundButton, SmartPad", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android", this, new java.lang.Object[] { p0, p1 });
		}
	}


	public SoundButton (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == SoundButton.class) {
			mono.android.TypeManager.Activate ("SmartPad.Utils.SoundButton, SmartPad", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2 });
		}
	}


	public SoundButton (android.content.Context p0, android.util.AttributeSet p1, int p2, int p3)
	{
		super (p0, p1, p2, p3);
		if (getClass () == SoundButton.class) {
			mono.android.TypeManager.Activate ("SmartPad.Utils.SoundButton, SmartPad", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2, p3 });
		}
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
