package crc64175a520abd881465;


public class ServerThread
	extends java.lang.Thread
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_run:()V:GetRunHandler\n" +
			"";
		mono.android.Runtime.register ("SmartPad.Services.ServerThread, SmartPad", ServerThread.class, __md_methods);
	}


	public ServerThread ()
	{
		super ();
		if (getClass () == ServerThread.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.ServerThread, SmartPad", "", this, new java.lang.Object[] {  });
		}
	}


	public ServerThread (java.lang.Runnable p0)
	{
		super (p0);
		if (getClass () == ServerThread.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.ServerThread, SmartPad", "Java.Lang.IRunnable, Mono.Android", this, new java.lang.Object[] { p0 });
		}
	}


	public ServerThread (java.lang.Runnable p0, java.lang.String p1)
	{
		super (p0, p1);
		if (getClass () == ServerThread.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.ServerThread, SmartPad", "Java.Lang.IRunnable, Mono.Android:System.String, mscorlib", this, new java.lang.Object[] { p0, p1 });
		}
	}


	public ServerThread (java.lang.String p0)
	{
		super (p0);
		if (getClass () == ServerThread.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.ServerThread, SmartPad", "System.String, mscorlib", this, new java.lang.Object[] { p0 });
		}
	}


	public ServerThread (java.lang.ThreadGroup p0, java.lang.Runnable p1)
	{
		super (p0, p1);
		if (getClass () == ServerThread.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.ServerThread, SmartPad", "Java.Lang.ThreadGroup, Mono.Android:Java.Lang.IRunnable, Mono.Android", this, new java.lang.Object[] { p0, p1 });
		}
	}


	public ServerThread (java.lang.ThreadGroup p0, java.lang.Runnable p1, java.lang.String p2)
	{
		super (p0, p1, p2);
		if (getClass () == ServerThread.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.ServerThread, SmartPad", "Java.Lang.ThreadGroup, Mono.Android:Java.Lang.IRunnable, Mono.Android:System.String, mscorlib", this, new java.lang.Object[] { p0, p1, p2 });
		}
	}


	public ServerThread (java.lang.ThreadGroup p0, java.lang.Runnable p1, java.lang.String p2, long p3)
	{
		super (p0, p1, p2, p3);
		if (getClass () == ServerThread.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.ServerThread, SmartPad", "Java.Lang.ThreadGroup, Mono.Android:Java.Lang.IRunnable, Mono.Android:System.String, mscorlib:System.Int64, mscorlib", this, new java.lang.Object[] { p0, p1, p2, p3 });
		}
	}


	public ServerThread (java.lang.ThreadGroup p0, java.lang.String p1)
	{
		super (p0, p1);
		if (getClass () == ServerThread.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.ServerThread, SmartPad", "Java.Lang.ThreadGroup, Mono.Android:System.String, mscorlib", this, new java.lang.Object[] { p0, p1 });
		}
	}

	public ServerThread (crc646f180076006ce2d3.LobbyActivity p0)
	{
		super ();
		if (getClass () == ServerThread.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.ServerThread, SmartPad", "SmartPad.Activities.LobbyActivity, SmartPad", this, new java.lang.Object[] { p0 });
		}
	}


	public void run ()
	{
		n_run ();
	}

	private native void n_run ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
