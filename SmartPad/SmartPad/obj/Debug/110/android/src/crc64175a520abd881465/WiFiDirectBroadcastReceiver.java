package crc64175a520abd881465;


public class WiFiDirectBroadcastReceiver
	extends android.content.BroadcastReceiver
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onReceive:(Landroid/content/Context;Landroid/content/Intent;)V:GetOnReceive_Landroid_content_Context_Landroid_content_Intent_Handler\n" +
			"";
		mono.android.Runtime.register ("SmartPad.Services.WiFiDirectBroadcastReceiver, SmartPad", WiFiDirectBroadcastReceiver.class, __md_methods);
	}


	public WiFiDirectBroadcastReceiver ()
	{
		super ();
		if (getClass () == WiFiDirectBroadcastReceiver.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.WiFiDirectBroadcastReceiver, SmartPad", "", this, new java.lang.Object[] {  });
		}
	}

	public WiFiDirectBroadcastReceiver (android.net.wifi.p2p.WifiP2pManager p0, android.net.wifi.p2p.WifiP2pManager.Channel p1, crc646f180076006ce2d3.LobbyActivity p2)
	{
		super ();
		if (getClass () == WiFiDirectBroadcastReceiver.class) {
			mono.android.TypeManager.Activate ("SmartPad.Services.WiFiDirectBroadcastReceiver, SmartPad", "Android.Net.Wifi.P2p.WifiP2pManager, Mono.Android:Android.Net.Wifi.P2p.WifiP2pManager+Channel, Mono.Android:SmartPad.Activities.LobbyActivity, SmartPad", this, new java.lang.Object[] { p0, p1, p2 });
		}
	}


	public void onReceive (android.content.Context p0, android.content.Intent p1)
	{
		n_onReceive (p0, p1);
	}

	private native void n_onReceive (android.content.Context p0, android.content.Intent p1);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
