﻿using SQLite;

namespace SmartPad.DataBase
{
    internal class BoardConf
    {
        [PrimaryKey, Unique]
        public int ID { get; set; }

        public int Rot { get; set; }

        public int Gruen { get; set; }

        public int Blau { get; set; }

        public int SoundID { get; set; }

        public bool Repeate { get; set; }

        public bool Stop { get; set; }
 
    }
}