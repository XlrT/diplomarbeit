﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartPad.Utils
{
    class SoundFile
    {
        public string name;
        public string path;
        public int id;

        public SoundFile(string name, string path)
        {
            this.name = name;
            this.path = path;
            id++;
        }
        //do i need this?
        /*public SoundFile(string name)
        {
            this.name = name;
            id++;
        }*/
    }
}