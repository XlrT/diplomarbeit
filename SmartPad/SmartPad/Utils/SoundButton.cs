﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SmartPad.DataBase;

namespace SmartPad.Utils
{
    class SoundButton : Button
    {
        public Sound sound;
        public bool repeat;
        public bool stop;
        public int sId;
        public int spId;

        public SoundButton(Context context, bool stop, bool repeat, Sound sound, int sId) : base(context)
        {
            this.sId = sId;
            this.sound = sound;
            this.repeat = repeat;
            this.stop = stop;
        }
        public SoundButton(Context context, bool stop, Sound sound, int sId) : base(context)
        {
            this.sId = sId;
            this.sound = sound;
            this.stop = stop;
            this.repeat = false;
        }
        public SoundButton(Context context, bool stop, int sId) : base(context)
        {
            this.sId = sId;
            this.stop = stop;
        }
    }
}