﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Net;
using SmartPad.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartPad.Services
{
    internal class ReadThread : Thread
    {
        private Socket socket;
        private LobbyActivity activity;

        public ReadThread(Socket socket, LobbyActivity activity)
        {
            this.socket = socket;
            this.activity = activity;
        }

        public override void Run()
        {
            byte[] buffer = new byte[1024];
            int bytes;
            while (true)
            {
                try
                {
                    bytes = socket.InputStream.Read(buffer, 0, buffer.Length);
                    if (bytes > 0)
                    {
                        var handler = new Handler(activity.MainLooper);
                        handler.Post(() =>
                        {
                            string message = Encoding.ASCII.GetString(buffer, 0, bytes);
                            Toast.MakeText(activity, message, ToastLength.Long).Show();
                        });
                    }
                }
                catch (Java.IO.IOException e)
                {
                    e.PrintStackTrace();
                }
            }
        }
    }
}