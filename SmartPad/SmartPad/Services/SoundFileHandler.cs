﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartPad.Utils;
using SmartPad.DataBase;
using Android.Media;
using System.IO;

namespace SmartPad.Services
{

    class SoundFileHandler
    {
        List<SoundFile> SoundFileList;
        MediaMetadataRetriever reader;

        public SoundFileHandler()
        {
            DataBaseHelper dbHelper = new DataBaseHelper();
            SoundFileList = new List<SoundFile>();
            string path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryMusic).ToString(); //veraltet in Andoird 11
            string[] fileArr = Directory.GetFiles(path); //Showcase using .Net Features in Andorid
            reader = new MediaMetadataRetriever();
            foreach (string s in fileArr)
            {
                reader.SetDataSource(s);
                //überprüfen die Metadaten der Datei auf eine Songtitel
                string name = reader.ExtractMetadata(MetadataKey.Title);
                if(name == null)
                {
                    SoundFileList.Add(new SoundFile(s, s));
                }
                else
                {
                    SoundFileList.Add(new SoundFile(name, s));
                }
            }
            var fields = typeof(Resource.Raw).GetFields();

        }
        public List<SoundFile> GetSoundFileList()
        {
            return SoundFileList;
        }
        public SoundFile GetSoundFileAt(int pos)
        {
            return SoundFileList[pos];
        }
    }
}