﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Net;
using SmartPad.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartPad.Services
{
    internal class ClientThread : Thread
    {
        private string host;
        private LobbyActivity activity;

        public ClientThread(string host, LobbyActivity activity)
        {
            this.host = host;
            this.activity = activity;
        }

        public override void Run()
        {
            try
            {
                Socket socket = new Socket(host, 8888);
                var handler = new Handler(activity.MainLooper);
                handler.Post(() =>
                {
                    Toast.MakeText(activity, "Fuck Yeah", ToastLength.Short).Show();
                });

                byte[] messageBytes = Encoding.ASCII.GetBytes("success: " + Android.OS.Build.Host);
                socket.OutputStream.Write(messageBytes, 0, messageBytes.Length);

                var readThread = new ReadThread(socket, activity);
                readThread.Start();
            }
            catch (Java.IO.IOException e)
            {
                e.PrintStackTrace();
            }
        }
    }
}