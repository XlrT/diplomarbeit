﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Net;
using SmartPad.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartPad.Services
{
    class ServerThread : Thread
    {
        private LobbyActivity activity;
        private ServerSocket serverSocket;

        public ServerThread( LobbyActivity activity )
        { 
            this.activity = activity;
            try { this.serverSocket = new ServerSocket(8888); }
            catch(Java.IO.IOException e) { e.PrintStackTrace(); }
            
        }
        public override void Run()
        {
            while(true)
            {
                try
                {
                    Socket client = serverSocket.Accept();
                    var handler = new Handler(activity.MainLooper);
                    handler.Post(() =>
                        { Toast.MakeText(activity, "A client has connected", ToastLength.Short).Show(); });
                    var readThread = new ReadThread(client, activity);
                    readThread.Start();
                }
                catch (Java.IO.IOException e)
                {
                    e.PrintStackTrace();                    
                }
            }
        }

    }
}