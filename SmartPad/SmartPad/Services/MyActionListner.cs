﻿using Android.Content;
using Android.Net.Wifi.P2p;
using Android.Widget;
using System;

namespace SmartPad.Activities
{
    internal class MyActionListner : Java.Lang.Object, WifiP2pManager.IActionListener
    {
        private readonly Context context;
        private readonly string status;
        private readonly Action action;

        public MyActionListner(Context context, string status, Action onSuccessAction)
        {
            this.context = context;
            this.status = status;
            this.action = onSuccessAction;
        }

        public void OnFailure(WifiP2pFailureReason reason)
        {
            Toast.MakeText(context, status + " Failed : " + reason, 
                ToastLength.Short).Show();
        }
        public void OnSuccess()
        {
            Toast.MakeText(context, status + " Initiated",
                ToastLength.Short).Show();
            action.Invoke();
        }
    }
}